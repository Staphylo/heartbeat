#!/usr/bin/env python3

import sys
import time
import json
import configparser

from flask import Flask, request, escape, g, Response, abort
app = Flask(__name__)

cfg = configparser.ConfigParser()

# {{{ Host management
class Hosts:
    def __init__(self, filename):
        self.filename = filename
        self.data = { }
        try:
            with open(filename, "r") as f:
                self.data = json.loads(f.read())
        except:
            with open(filename, "w+") as f:
                f.write(json.dumps(self.data))

    def commit(self):
        with open(self.filename, "w+") as f:
            f.write(json.dumps(self.data))

    def append(self, host, ip):
        self.data[host] = { "ip" : ip, "last" : time.time() }
        self.commit()

    def remove(self, host):
        del(self.data[host])
        self.commit()

    def get(self, host=None):
        if host:
            return self.data[host]
        return self.data;
# }}}

hosts = None

@app.before_request
def before_request():
    g.h = hosts

# {{{ Gathering data
@app.route("/")
def root():
    res = '''
        <!DOCTYPE html>
        <html>
            <head>
                <title>Host list</title>
                <style>
                table { width: 100%; }
                .alive { background-color: #EEFFEE; }
                .dead { background-color: #FFEEEE; }
                </style>
            </head>
            <body>
                <table>
    '''
    data = g.h.get()
    for host, desc in data.items():
        last = time.time() - desc["last"];
        attr = "alive"
        if last > cfg.getint("timeout"):
            attr = "dead"
        res += '<tr class="%s"><td>%s</td><td>%s</td><td>%s</td></tr>' % (attr,
                attr, host, desc["ip"])
    res += '''
                </table>
            </body>
        </html>
    '''
    return res;

@app.route("/json")
def raw():
    data = json.dumps(g.h.get(), sort_keys=True, indent=4, separators=(',', ': '))
    return Response(data, content_type="text/plain;charset=UTF-8")

@app.route("/get/<host>")
def get(host):
    data = g.h.get()
    try:
        data = data[host]["ip"]
    except:
        abort(404)
    return Response(data, content_type="text/plain;charset=UTF-8")

@app.route("/status/<host>")
def status(host):
    data = g.h.get()
    try:
        data = data[host]["last"]
    except:
        abort(404)
    if time.time() - data > cfg.getint("timeout"):
        data = "dead"
    else:
        data = "alive"
    return Response(data, content_type="text/plain;charset=UTF-8")
# }}}

# {{{ Sending information
def update_dyndns(host, ip):
    pass

@app.route("/beat/<host>")
def beat_get(host):
    if host not in g.h.get().keys():
        abort(404)
    ip = request.remote_addr
    oldip = g.h.get()[host]["ip"]
    if oldip != ip:
        update_dyndns(host, ip)
    g.h.append(host, ip)
    return ip

def verify_password(password):
    return password == cfg.get("password")

@app.route("/create/<host>/<password>")
def create(host, password):
    if not verify_password(password):
        abort(403)
    g.h.append(host, request.remote_addr)
    return "ok"

@app.route("/remove/<host>/<password>")
def remove(host, password):
    if not verify_password(password):
        abort(403)
    if host not in g.h.get().keys():
        abort(404)
    g.h.remove(host)
    return "ok"
# }}}

# {{{ Extra features
@app.route("/headers")
def headers():
    data = ""
    for key, value in request.headers:
        if value:
            data += "%s: %s\n" % (key, value)
    return Response(data, content_type="text/plain;charset=UTF-8")
# }}}

@app.errorhandler(404)
def error_not_found(error):
    return "404"

@app.errorhandler(403)
def error_forbidden(error):
    return "403"

if __name__ == "__main__":
    cfg['DEFAULT'] = {
        'ip': "127.0.0.1",
        'port': 5000,
        'debug': True,
        'timeout': 660,
        'hostfile': "data.json",
        'password': "default"
    }

    if len(sys.argv) > 1:
        cfg.read(sys.argv[1])

    cfg = cfg['DEFAULT']

    hosts = Hosts(cfg.get("hostfile"))

    app.run(host=cfg.get("ip"), port=cfg.getint("port"),
            debug=cfg.getboolean("debug"))
