#!/bin/sh

host=
interval=600

if [ -z "$1" ]; then
    echo "usage: $0 hostname"
    exit 1
fi

hostname=$1

while true; do
    echo -n "sending hearbeat... "
    wget -O - -q http://$host/beat/$hostname
    echo ""
    sleep $interval
done
