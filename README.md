Heartbeat:
==========

Server:
-------

Server written in python3 using Flask
usage `server.py [config]`


Config:
-------

The following is the default configuration

    [DEFAULT]

    ip       = 127.0.0.1
    port     = 5000
    debug    = True
    timeout  = 660
    hostfile = data.json
    password = default


Client:
-------

Any GET request do the job

Also `heartbeat.sh` is provided which send a beat to the host each 10 minutes


Routes:
-------

**/** Display the current state of the machines

**/json** Give the raw data in json

**/get/<host>** return the ip address of the given host

**/status/<host>** say if the host is dead or alive

**/beat/<host>** send a heartbeat for the host

**/create/<host>/<password>** create a new host ready for a heartbeat

**/remove/<host>/<password>** remove a host entry

**/headers** dump the headers sent by the navigator


Security:
---------

There is no great security.
The goal is to be easy to use.

the current vulnerabilities are:

 - mitm : owned, nothing is encrypted
 - bruteforce : limit attempts
 - beat : use a token per host given at creation



Why:
----

 - Host monitoring
 - Dynamic ip addressing
 - Update dns zone when a host change ip **[TODO]**
